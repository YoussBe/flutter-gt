import 'package:flutter/material.dart';

class ConstraintsScreen extends StatelessWidget {
  const ConstraintsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Card(
          child: Container(
            width: 40,
            child: FittedBox(
              child: Text(
                "Hello",
                style: TextStyle(fontSize: 70),
              ),
            ),
          ),
        ),
      )),
    );
  }
}
