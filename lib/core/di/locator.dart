import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc_new.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => AuthBloc());
}
