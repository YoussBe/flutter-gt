import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/screens/home/home_screen.dart';
import 'package:flutter_application_1/app/screens/login/login_screen.dart';
import 'package:flutter_application_1/app/screens/savings/saving_screen.dart';
import 'package:flutter_application_1/app/screens/splash/splash_screen.dart';

//constantes noms de routes
const kSplashRoute = '/';
const kLoginRoute = '/login';
const kHomeRoute = '/home';
const kSavingRoute = '/saving';

// noms -> Widgets
final Map<String, WidgetBuilder> kRoutes = {
  kSplashRoute: (_) => SplashScreen(),
  kLoginRoute: (_) => LoginScreen(),
  kHomeRoute: (_) => HomeScreen(),
  kSavingRoute: (_) => SavingsScreen(),
};

onGenerateRoute(settings) {
  if (settings.name == kSavingRoute) {
    String? myEquation = settings.arguments;
    print(myEquation);
    return MaterialPageRoute(
        builder: (_) => SavingsScreen(
              lastEquation: myEquation,
            ));
  } else if (settings.name != null) {
    return MaterialPageRoute(builder: kRoutes[settings.name]!);
  } else {
    return null;
  }
}
