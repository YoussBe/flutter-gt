import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc.dart';
import 'package:flutter_application_1/app_routes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app/modules/auth/bloc/auth_bloc_new.dart';
import 'core/di/locator.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final AuthBloc authBloc = locator<AuthBloc>();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      builder: (_, widget) {
        return MultiBlocProvider(
            providers: [BlocProvider<AuthBloc>(create: (_) => authBloc)],
            child: widget ?? Container());
      },
      //home: CounterPage(),
      initialRoute: kSplashRoute,
      onGenerateRoute: (settings) => onGenerateRoute(settings),
      routes: kRoutes,
    );
  }
}
