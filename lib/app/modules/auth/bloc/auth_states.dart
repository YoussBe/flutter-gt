import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class InitialAuthState extends AuthState {
  @override
  List<Object?> get props => [];
}

class LoginSuccess extends AuthState {
  const LoginSuccess({required this.token, this.username});
  final String token;
  final String? username;
  @override
  List<Object?> get props => [token, username];
}

class LoginLoading extends AuthState {
  @override
  List<Object?> get props => [];
}

class LoginFailed extends AuthState {
  const LoginFailed({required this.error});
  final String error;
  @override
  List<Object?> get props => [error];
}

class LogoutSucess extends AuthState {
  @override
  List<Object?> get props => [];
}
