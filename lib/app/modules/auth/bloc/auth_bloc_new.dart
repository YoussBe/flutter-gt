import 'package:flutter_application_1/app/modules/auth/bloc/auth_events.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_states.dart';
import 'package:flutter_application_1/app/modules/auth/repository/auth_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(InitialAuthState()) {
    on<Login>(_onLogin);
  }

  final AuthRepository authRepository = AuthRepository();

  void _onLogin(
    Login event,
    Emitter<AuthState> emit,
  ) async {
    emit(LoginLoading());

    try {
      Map<String, String> results = await authRepository.login(
          email: event.email, password: event.password);
      emit(
        LoginSuccess(token: results["token"]!, username: results["username"]),
      );
    } catch (error) {
      emit(LoginFailed(error: error.toString()));
    }
  }
}
