import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class Login extends AuthEvent {
  final String email;
  final String password;

  const Login({required this.email, required this.password});

  @override
  List<Object?> get props => [email, password];
}

class Logout extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class CheckAuth extends AuthEvent {
  @override
  List<Object?> get props => [];
}
