import 'package:flutter_application_1/app/modules/auth/bloc/auth_events.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_states.dart';
import 'package:flutter_application_1/app/modules/auth/repository/auth_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthBloc2 extends Bloc<AuthEvent, AuthState> {
  AuthBloc2() : super(InitialAuthState());
  final AuthRepository _repo = AuthRepository();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is Login) {
      yield LoginLoading();
      try {
        Map<String, String> results =
            await _repo.login(email: event.email, password: event.password);
        yield LoginSuccess(
            token: results["token"]!, username: results["username"]);
      } catch (e) {
        print(e);
        yield LoginFailed(error: e.toString());
      }
    }
  }
}
