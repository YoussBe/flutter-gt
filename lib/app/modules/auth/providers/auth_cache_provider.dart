import 'package:flutter_application_1/core/cache/cache_helper.dart';

class AuthCacheProvider {
  // communiquer directement avec le cache (plugin shared pref)
  final CacheHelper _helper = CacheHelper();

  saveToken(String token) {
    _helper.save(key: 'auth_token', value: token);
  }

  Future<String?> loadToken() async {
    String? token = await _helper.read(key: 'auth_token');

    return token;
  }

  logout() {
    _helper.clear();
  }
}
