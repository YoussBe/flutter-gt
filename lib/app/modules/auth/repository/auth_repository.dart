import 'package:flutter_application_1/app/modules/auth/providers/auth_cache_provider.dart';

class AuthRepository {
  final AuthCacheProvider _cache = AuthCacheProvider();

  saveToken(String token) {
    _cache.saveToken(token);
  }

  Future<Map<String, String>> login(
      {required String email, required String password}) async {
    if (email != "youssef@gmail.com") {
      // simuler une erreur
      return Future.delayed(const Duration(seconds: 2), () {
        throw Exception("Wrong email");
      });
    } else {
      // simuler un succés
      return Future.delayed(const Duration(seconds: 2), () async {
        return {"token": "myToken", "username": "Youssef"};
      });
    }
  }

  Future<String?> loadTokenFromCache() async {
    return await _cache.loadToken();
  }

  logout() {
    _cache.logout();
  }
}
