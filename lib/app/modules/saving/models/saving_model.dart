class Saving {
  Saving(
      {this.id,
      required this.title,
      required this.subtitle,
      required this.total});
  int? id;
  final String title;
  final String subtitle;
  final String total;

  factory Saving.fromJson(Map<String, dynamic> json) {
    return Saving(
        id: json['id'],
        title: json["title"],
        subtitle: json["subtitle"],
        total: json['total']);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    json['id'] = id;
    json['title'] = title;
    json['subtitle'] = subtitle;
    json['total'] = total;
    return json;
  }
}
