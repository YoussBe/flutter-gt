import 'dart:io';

import 'package:flutter_application_1/app/modules/saving/models/saving_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SavingDBProvider {
  final _databaseName = "my_calculator";
  final _table = "saving";

  Database? _database;

  Future<Database?> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDatabse();
      return _database;
    }
  }

  initDatabse() async {
    return await openDatabase(join(await getDatabasesPath(), _databaseName),
        onCreate: (db, version) {
      return db.execute('''
      CREATE TABLE $_table (
        id INTEGER PRIMARY KEY,
        title TEXT NOT NULL,
        subtitle TEXT,
        total TEXT NOT NULL
      )
        ''');
    }, version: 1);
  }

  // CRUD

  Future<List<Map<String, dynamic>>> queryAllSavings() async {
    Database? db = await database;
    return await db!.query(_table);
  }

  Future<Saving> insert(Saving saving) async {
    Database? db = await database;
    saving.id = await db!.insert(_table, saving.toJson());
    print(saving);
    return saving;
  }

  Future<Saving> update(Saving saving) async {
    Database? db = await database;
    await db!.update(_table, saving.toJson(),
        where: 'id = ?', whereArgs: [saving.id]);
    return saving;
  }
}
