import 'package:flutter_application_1/app/modules/saving/models/saving_model.dart';
import 'package:flutter_application_1/app/modules/saving/providers/saving_db_provider.dart';

class SavingRepository {
  final SavingDBProvider _db = SavingDBProvider();

  Future<List<Saving>> getAllSavings() async {
    List<Map<String, dynamic>> result = await _db.queryAllSavings();
    return result.map((e) => Saving.fromJson(e)).toList();
  }

  Future<Saving> addSaving(Saving saving) async {
    return await _db.insert(saving);
  }

  Future<Saving> updateSaving(Saving saving) async {
    return await _db.update(saving);
  }
}
