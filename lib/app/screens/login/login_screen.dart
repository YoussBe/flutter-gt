import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc_new.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_events.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_states.dart';
import 'package:flutter_application_1/app/modules/auth/repository/auth_repository.dart';
import 'package:flutter_application_1/app_routes.dart';
import 'package:flutter_application_1/core/di/locator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

final _formKey = GlobalKey<FormState>();

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool passwordObscuring = true;
  AutovalidateMode _autovalidateMode = AutovalidateMode.disabled;
  final AuthBloc authBloc = locator<AuthBloc>();

  showPassword() {
    passwordObscuring = false;
    setState(() {});
  }

  navigateToHome() {
    Navigator.pushReplacementNamed(context, kHomeRoute);
  }

  login() {
    if (_formKey.currentState!.validate()) {
      //cacher le clavier
      FocusManager.instance.primaryFocus?.unfocus();

      authBloc.add(Login(
          email: _emailController.text, password: _passwordController.text));
    } else {
      _autovalidateMode = AutovalidateMode.always;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          "Login",
                          style: TextStyle(color: Colors.black),
                        ),
                        const Text(
                          "Please log in to your account",
                          style: TextStyle(color: Colors.black),
                        ),
                        Form(
                          autovalidateMode: _autovalidateMode,
                          key: _formKey,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 40, vertical: 10),
                            child: Column(
                              children: [
                                TextFormField(
                                  decoration:
                                      InputDecoration(label: Text("Email")),
                                  controller: _emailController,
                                ),
                                TextFormField(
                                  decoration: InputDecoration(
                                      suffixIcon: IconButton(
                                          onPressed: () => showPassword(),
                                          icon: Icon(Icons.remove_red_eye)),
                                      label: Text("Mot de passe")),
                                  controller: _passwordController,
                                  obscureText: passwordObscuring,
                                  validator: (content) {
                                    if (content != null &&
                                        content.length <= 4) {
                                      return "Votre mot de passe doit avoir une taille > 4";
                                    }
                                    return null;
                                  },
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BlocConsumer<AuthBloc, AuthState>(
                                    listener: (context, state) {
                                      if (state is LoginSuccess) {
                                        navigateToHome();
                                      }
                                    },
                                    builder: (context, state) {
                                      if (state is LoginLoading) {
                                        return const Center(
                                            child: CircularProgressIndicator());
                                      } else {
                                        return Column(
                                          children: [
                                            (state is LoginFailed)
                                                ? const Center(
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(8.0),
                                                      child: Text(
                                                        "Authentification échouée, verifier vos identifiants",
                                                        style: TextStyle(
                                                            color: Colors.red),
                                                      ),
                                                    ),
                                                  )
                                                : Container(),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: ElevatedButton(
                                                  onPressed: () {
                                                    login();
                                                  },
                                                  child: const Text('Login')),
                                            ),
                                          ],
                                        );
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
