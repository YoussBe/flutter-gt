import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_bloc_new.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_events.dart';
import 'package:flutter_application_1/app/modules/auth/bloc/auth_states.dart';
import 'package:flutter_application_1/app/modules/auth/repository/auth_repository.dart';
import 'package:flutter_application_1/app/modules/saving/models/saving_model.dart';
import 'package:flutter_application_1/app/modules/saving/repository/saving_repository.dart';
import 'package:flutter_application_1/core/di/locator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:function_tree/function_tree.dart';
import 'package:flutter_application_1/app_routes.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String currentNumber = "0";
  String equation = '';
  bool isError = false;
  final SavingRepository _savingRepo = SavingRepository();
  final AuthBloc authBloc = locator<AuthBloc>();

  List<List<Map<String, dynamic>>> keyboardTouches = [
    [
      {"content": "C", "isBig": false, "isDark": false},
      {"content": "(", "isBig": false, "isDark": false},
      {"content": ")", "isBig": false, "isDark": false},
      {"content": "/", "isBig": false, "isDark": false}
    ],
    [
      {"content": "7", "isBig": false, "isDark": true},
      {"content": "8", "isBig": false, "isDark": true},
      {"content": "9", "isBig": false, "isDark": true},
      {"content": "*", "isBig": false, "isDark": false}
    ],
    [
      {"content": "4", "isBig": false, "isDark": true},
      {"content": "5", "isBig": false, "isDark": true},
      {"content": "6", "isBig": false, "isDark": true},
      {"content": "-", "isBig": false, "isDark": false}
    ],
    [
      {"content": "1", "isBig": false, "isDark": true},
      {"content": "2", "isBig": false, "isDark": true},
      {"content": "3", "isBig": false, "isDark": true},
      {"content": "+", "isBig": false, "isDark": false}
    ],
    [
      {"content": "0", "isBig": true, "isDark": true},
      {"content": ".", "isBig": false, "isDark": true},
      {"content": "=", "isBig": false, "isDark": false},
    ]
  ];

  evaluateEquation() {
    try {
      currentNumber = equation.interpret().toString();
      saveToDb();
    } catch (e) {
      isError = true;
      setState(() {});
    }
  }

  saveToDb() {
    _savingRepo.addSaving(
        Saving(title: equation, total: currentNumber, subtitle: 'a new entry'));
  }

  addElementToEquation(String element) {
    if (element == 'C') {
      isError = false;

      equation = '';
      currentNumber = "0";
    } else if (element == '=') {
      evaluateEquation();
    } else {
      equation = equation + element;
    }
    setState(() {});
  }

  Future<bool> popCallback() {
    Navigator.of(context).pop("Hello 2");
    return Future.value(true);
  }

  logout() async {
    authBloc.add(Logout());
    Navigator.of(context)
        .pushNamedAndRemoveUntil(kSplashRoute, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: popCallback,
      child: Scaffold(
        appBar: AppBar(
          foregroundColor: Colors.black,
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: BlocBuilder<AuthBloc, AuthState>(builder: (_, state) {
            if (state is LoginSuccess) {
              return Text(state.username!);
            } else {
              return Container();
            }
          }),
          leading: Container(),
          actions: [
            Stack(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(kSavingRoute);
                    },
                    icon: const Icon(Icons.save)),
                Positioned(
                  left: 8,
                  top: 8,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.red,
                    ),
                    height: 15,
                    width: 15,
                    child: Center(
                      child: Text('8',
                          style: TextStyle(fontSize: 10, color: Colors.white)),
                    ),
                  ),
                ),
              ],
            ),
            Stack(
              children: [
                IconButton(
                    onPressed: () {
                      logout();
                    },
                    icon: Icon(Icons.logout)),
              ],
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(
            //pousser tous les elements de la grande colonne à droite
            crossAxisAlignment: CrossAxisAlignment.end,
            // contrôler l'alignement des children
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                onDoubleTap: () {
                  Navigator.pushNamed(context, kSavingRoute,
                      arguments: equation);
                },
                child: CalculDisplay(
                  currentNumber: currentNumber,
                  equation: equation,
                ),
              ),
              isError
                  ? Text(
                      'Please Correct your equation',
                      style: TextStyle(color: Colors.red),
                    )
                  : Container(),
              Column(
                children: keyboardTouches
                    .map((line) => Row(
                        children: line
                            .map((element) => CalcButton(
                                content: element["content"],
                                onTap: addElementToEquation,
                                isBig: element["isBig"],
                                isDark: element["isDark"]))
                            .toList()))
                    .toList(),
              ),
              /*  Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CalcButton(content: "C", isDark: false, onTap: addNumber),
                      CalcButton(
                        content: "(",
                        isDark: false,
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "(",
                        isDark: false,
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "/",
                        isDark: false,
                        onTap: addNumber,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CalcButton(
                        content: "7",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "8",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "9",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "X",
                        isDark: false,
                        onTap: addNumber,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CalcButton(
                        content: "4",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "5",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "6",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "-",
                        isDark: false,
                        onTap: addNumber,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CalcButton(
                        content: "1",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "2",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "3",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: "+",
                        isDark: false,
                        onTap: addNumber,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CalcButton(
                        isBig: true,
                        content: "0",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        content: ".",
                        onTap: addNumber,
                      ),
                      CalcButton(
                        isDark: false,
                        content: "=",
                        onTap: addNumber,
                      ),
                    ],
                  ),
                ],
              ) */
            ],
          ),
        ),
      ),
    );
  }
}

class CalculDisplay extends StatelessWidget {
  const CalculDisplay({
    Key? key,
    required this.currentNumber,
    this.equation = "",
  }) : super(key: key);

  final String currentNumber;
  final String equation;

  @override
  Widget build(BuildContext context) {
    return Column(
      //pousser tous les elements de la petite colonne de textes à droite
      crossAxisAlignment: CrossAxisAlignment.end,

      children: [
        Text(
          equation,
          style: TextStyle(color: Colors.grey),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          currentNumber,
          style: TextStyle(fontSize: 60, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}

class CalcButton extends StatelessWidget {
  const CalcButton(
      {Key? key,
      required this.content,
      this.isDark = true,
      this.isBig = false,
      required this.onTap})
      : super(key: key);

  final String content;
  //
  final bool isDark;
  final bool isBig;
  final Function(String) onTap;

  @override
  Widget build(BuildContext context) {
    var swidth = MediaQuery.of(context).size.width;
    return Expanded(
      flex: isBig ? 2 : 1,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: InkWell(
          borderRadius: BorderRadius.circular(15),
          splashColor: Colors.red,
          onTap: () {
            onTap(content);
          },
          child: Ink(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              borderRadius: BorderRadius.circular(15),
              color: isDark ? Colors.black : Colors.white,
            ),
            height: swidth / 7,
            child: Center(
              child: Text(
                content,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: isDark ? Colors.white : Colors.black,
                    fontSize: 20),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
