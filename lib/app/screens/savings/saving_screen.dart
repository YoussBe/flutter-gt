import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/saving/repository/saving_repository.dart';
import '../../modules/saving/models/saving_model.dart';
import 'widgets/savings_list_element.dart';

class SavingsScreen extends StatefulWidget {
  const SavingsScreen({Key? key, this.lastEquation}) : super(key: key);

  final String? lastEquation;

  @override
  State<SavingsScreen> createState() => _SavingsScreenState();
}

class _SavingsScreenState extends State<SavingsScreen> {
  final SavingRepository _savingRepo = SavingRepository();

  List<Saving> savingList = [];
  bool isLoading = true;

  @override
  void initState() {
    getAllSavings();

    super.initState();
  }

  getAllSavings() async {
    Future.delayed(Duration(seconds: 2), () async {
      savingList = await _savingRepo.getAllSavings();
      isLoading = false;
      setState(() {});
      print(savingList);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          foregroundColor: Colors.black,
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text("Calculs récents"),
          actions: [
            Image.asset(
              'assets/images/logo.png',
              height: 20,
            )
          ],
        ),
        body: isLoading
            ? Center(child: const CircularProgressIndicator())
            : ListView.builder(
                itemCount: savingList.length,
                itemBuilder: (_, position) {
                  return ListElement(saving: savingList[position]);
                }));

    /* ListView(
            children: savingList
                .map((element) => ListElement(saving: element))
                .toList())); */
  }
}
