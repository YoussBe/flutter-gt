import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/saving/models/saving_model.dart';

class SavingSelectionDialog extends StatelessWidget {
  const SavingSelectionDialog({Key? key, required this.saving})
      : super(key: key);

  final Saving saving;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Close")),
          Text(
            saving.title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
          ),
          Text(
            saving.subtitle,
            style: TextStyle(color: Colors.grey, fontSize: 14),
          ),
          Text(
            saving.total.toString(),
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 70),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: Text("Change color")),
        ],
      ),
    );
    ;
  }
}
