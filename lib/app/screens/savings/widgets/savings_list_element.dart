import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/screens/savings/dialogs/savings_selection_dialog.dart';
import 'package:flutter_application_1/app/modules/saving/models/saving_model.dart';

class ListElement extends StatefulWidget {
  ListElement({
    required this.saving,
    Key? key,
  }) : super(key: key);

  final Saving saving;

  @override
  State<ListElement> createState() => _ListElementState();
}

class _ListElementState extends State<ListElement> {
  bool? isSelected = false;

  showDetailsDialog(context) async {
    var resp = await showDialog(
        context: context,
        builder: (context) {
          return Dialog(
              child: SavingSelectionDialog(
            saving: widget.saving,
          ));
        });

    if (resp == true) {
      isSelected = true;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDetailsDialog(context);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.saving.title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            ),
            Text(
              widget.saving.subtitle,
              style: TextStyle(color: Colors.grey, fontSize: 14),
            ),
            Text(
              widget.saving.total.toString(),
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 70,
                  color: isSelected == true ? Colors.blue : Colors.black),
            ),
          ],
        ),
      ),
    );
  }
}
