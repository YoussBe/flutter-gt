import 'package:flutter/material.dart';
import 'package:flutter_application_1/app/modules/auth/repository/auth_repository.dart';
import 'package:flutter_application_1/app_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String info = '';
  final AuthRepository _authRepo = AuthRepository();

  navigateToHome(context) async {
    Navigator.pushReplacementNamed(context, kHomeRoute);
    print(info);
  }

  navigateToLogin(context) async {
    Navigator.pushReplacementNamed(context, kLoginRoute);
    print(info);
  }

  @override
  void initState() {
    super.initState();

    print("This widget is lunched");
    getAuth();
  }

  loadData() {
    Future.delayed(Duration(seconds: 2),
        () => Navigator.of(context).pushReplacementNamed(kLoginRoute));
  }

  getAuth() async {
    var userToken = await _authRepo.loadTokenFromCache();
    print("the token is: $userToken");
    if (userToken != null) {
      navigateToHome(context);
    } else {
      navigateToLogin(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logo.png'),
            Text(
              "My\nCalculator",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
            ),
            CircularProgressIndicator()
          ],
        ),
      ),
    );
  }
}
